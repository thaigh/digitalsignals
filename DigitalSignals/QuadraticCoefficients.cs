﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSignals
{
    /// <summary>
    /// Defines the coefficients for a quadratic equation of the form
    /// Ax^2 + Bx + C
    /// </summary>
    public class QuadraticCoefficients
    {
        public double A { get; set; }
        public double B { get; set; }
        public double C { get; set; }


        /// <summary>
        /// Find the maximum quadratic function value around a local maxima using the quadratic function's coefficients
        /// </summary>
        /// <param name="coeffs">The quadratic function's coefficients</param>
        /// <returns>The q(x) value for the quadratic function that gives the local maxima</returns>
        public double FindMaxima()
        {
            // Derivative of parabola
            // y' = 2Ax + B
            // Solve for x where y' = 0 (i.e. Maxima)
            // 0 = 2Ax + B
            // -B = 2Ax
            // x = -B / 2A
            return -B / (2.0 * A);
        }

        /// <summary>
        /// Determines the coefficients of the quadratic curve using three sequential points on a quadratic curve
        /// </summary>
        /// <param name="y0">The quadratic function value of q(x0)</param>
        /// <param name="y1">The quadratic function value of q(x1)</param>
        /// <param name="y2">The quadratic function value of q(x2)</param>
        /// <returns>The coefficients {A, B, C} that define the quadratic function</returns>
        public static QuadraticCoefficients DetermineCoefficients(double y0, double y1, double y2)
        {
            // Using the standard generic formula for a quadratic equation:
            // y = Ax^2 + Bx + C
            //
            // y0 = A(x0)^2 + B(x0) + C
            // y1 = A(x1)^2 + B(x1) + C
            // y2 = A(x2)^2 + B(x2) + C
            //
            // Using x0 = -1, x1 = 0, x2 = +1:
            //
            // y0 = A(x0)^2 + B(x0) + C
            //    = A(-1)^2 + B(-1) + C
            //    = A - B + C
            // y1 = A(x1)^2 + B(x1) + C
            //    = A(0)^2 + B(0) + C
            //    = C
            // y2 = A(x2)^2 + B(x2) + C
            //    = A(1)^2 + B(1) + C
            //    = A + B + C
            //
            // Solving:
            //
            // y2 - y0 = (A + B + y1) - (A - B + y1)
            //         = (A - A) + (B + B) + (y1 - y1)
            //         = 2B
            // Hence: B = (y2 - y0) / 2
            //
            // Solving:
            //
            // y0 = A - B + C
            // y0 + B - C = A
            // A = y0 - C + B
            // A = y0 - y1 + (y2 - y0) / 2
            //
            // Thus:
            // A = y0 - y1 + (y2 - y0) / 2
            // B = (y2 - y0) / 2
            // C = y1

            double a = y0 - y1 + (y2 - y0) / 2.0;
            double b = (y2 - y0) / 2.0;
            double c = y1;

            return new QuadraticCoefficients { A = a, B = b, C = c };
        }

        public bool PointsAreColinear()
        {
            // A = 0 implies quadratic is of the form 0x^2 + Bx + C
            // i.e. Bx + C which is a linear function
            return Math.Abs(A) < double.Epsilon;
        }

        /// <summary>
        /// Find the maximum quadratic function value around a local maxima
        /// </summary>
        /// <param name="y">Three sequential points that surround the local maxima</param>
        /// <returns>The q(x) value for the quadratic function that gives the local maxima</returns>
        public static double FindMaxima(double[] y)
        {
            if (y.Length != 3)
                throw new Exception("y array length must be 3 to find quadratic maxima");

            return FindMaxima(y[0], y[1], y[2]);
        }

        /// <summary>
        /// Find the maximum quadratic function value around a local maxima using three sequential points that surround the local maxima
        /// </summary>
        /// <param name="y0">A point on the left side of the maxima</param>
        /// <param name="y1">A point close to the maxima</param>
        /// <param name="y2">A point on the right side of the maxima</param>
        /// <returns>The q(x) value for the quadratic function that gives the local maxima</returns>
        public static double FindMaxima(double y0, double y1, double y2)
        {
            QuadraticCoefficients coeffs = QuadraticCoefficients.DetermineCoefficients(y0, y1, y2);

            if (coeffs.PointsAreColinear()) return y1; // Points are co-linear. Return middle point

            return coeffs.FindMaxima();
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace DigitalSignals
{
    static class LinqExtensions
    {
        /// <summary>
        /// Calculates the standard deviation of a set of numbers
        /// </summary>
        /// <param name="series">The series</param>
        /// <returns>The standard deviation of the series</returns>
        public static double StandardDeviation(this IEnumerable<double> series)
        {
            //http://stackoverflow.com/a/5336708/2442468
            double average = series.Average();
            double sumOfSquaresOfDifferences = series.Select(val => (val - average) * (val - average)).Sum();
            double sd = Math.Sqrt(sumOfSquaresOfDifferences / series.Count());
            return sd;
        }
    }
}

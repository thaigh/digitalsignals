﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSignals
{
    public static class IntExtensions
    {
        /// <summary>
        /// Calculates the mathematical modulo of this integer, mod the modulo value.
        /// Supports negative input values
        /// </summary>
        /// <param name="val">This value</param>
        /// <param name="modulo">The modulus</param>
        /// <returns>A number between 0 and modulo, representing the modded value</returns>
        public static int Modulo (this int val, int modulo)
        {
            int remainder = val % modulo;
            return (remainder < 0) ? remainder + modulo : remainder;
        }
    }
}

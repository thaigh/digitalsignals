﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSignals
{
    /// <summary>
    /// Captures information about a result from digital signal processing
    /// </summary>
    public class CorrelationResult
    {
        /// <summary>
        /// The delay at which the function was shifted when performing the correlation
        /// </summary>
        public int Delay { get; set; }

        /// <summary>
        /// The correlation factor between the two signals
        /// </summary>
        public double Correlation { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSignals
{
    /// <summary>
    /// Provides an API for Digital Signal Processing
    /// </summary>
    public static class DSP
    {
        public static double NormalisedCrossCorrelationCoefficient(IEnumerable<double> xSeries, IEnumerable<double> ySeries, int delay, CorrelationMode mode = CorrelationMode.IgnoreOutOfRange)
        {
            // https://en.wikipedia.org/wiki/Cross-correlation#Normalized_cross-correlation
            // "...the images can be first normalized. This is typically done at every step by subtracting the mean and dividing by the standard deviation"
            
            // Calculate:
            // 1/n * ( Sum(0,n) => ( x(i) - mean_x ) * (y(i) - mean_y) ) / ( StandardDeviation(x) * StandardDeviation(y) )

            // Calculate the mean (or average) of the two series
            double meanX = xSeries.Average();
            double meanY = ySeries.Average();

            // Calculate the Standard Deviation for the two series
            double standardDevX = xSeries.StandardDeviation();
            double standardDevY = ySeries.StandardDeviation();

            // Calculate the denominator
            double denominator = Math.Sqrt(standardDevX * standardDevY);

            // If denominator is zero, return empty list
            if (Math.Abs(denominator) < double.Epsilon) return 0;

            // Convert IEnumerable to array to provide indexing capability
            double[] xArray = xSeries.ToArray();
            double[] yArray = ySeries.ToArray();

            double normalisedCorrelationSeries = 0;
            for (int i = 0; i < yArray.Length; i++)
            {
                int delayedIndex = i + delay; // Start lagged, then eventually lead

                // CorrelationVal will be null if we use CorrelationMode.IgnoreOutOfRange and the index was out of range
                double? correlationVal = CalculateCorrelationSeriesValue(xArray, yArray, i, delayedIndex, meanX, meanY, mode);
                double normalisedCorrelation = (correlationVal == null) ? 0 : correlationVal.Value / denominator;
                normalisedCorrelationSeries += normalisedCorrelation;
            }

            return normalisedCorrelationSeries / yArray.Length;
        }

        public static IEnumerable<double> Convolution(IEnumerable<double> xSeries, IEnumerable<double> ySeries)
        {
            // http://stackoverflow.com/a/8425094/2442468

            // For convolution, we "flip" the ySeries and read it backwards.
            // We shift ySeries along xSeries (which stays constant) to find the convolution profile

            // Convert IEnumerable to array to allow for indexing
            double[] xList = xSeries.ToArray();
            double[] yList = ySeries.ToArray();

            // Calculate convolution result length
            int xSeriesLength = xList.Length;
            int ySeriesLength = yList.Length;
            int convolutionLength = xSeriesLength + ySeriesLength + 1;
            double[] conv = new double[convolutionLength];

            for (int i = 0; i < convolutionLength; i++)
            {
                int min = (i >= ySeriesLength - 1) ? (i - (ySeriesLength - 1)) : 0;
                int max = (i < xSeriesLength - 1) ? i : (xSeriesLength - 1);

                // Integrate the area between the two curves
                for (int j = min; j <= max; j++)
                {
                    double convolutionValue = xList[j] * yList[i - j];
                    conv[i] = convolutionValue;
                }
            }

            return conv;
        }

        /// <summary>
        /// Calculates the cross correlation of a given signal with itself when the signal is shifted by a given delay
        /// </summary>
        /// <param name="xSeries">The signal to auto correlate.</param>
        /// <param name="delay">How far to shift the signal when computing correlation coefficientThe </param>
        /// <param name="mode">How to process the correlation for edge cases</param>
        /// <returns>A the correlation coefficient between the original signal and itself when one is shifted by the given delay</returns>
        public static double AutoCorrelationCoefficient(IEnumerable<double> xSeries, int delay, CorrelationMode mode = CorrelationMode.IgnoreOutOfRange)
        {
            // Use Paul Bourke Cross Correlation function
            return PaulBourkeCrossCorrelationCoefficient(xSeries, xSeries, delay, mode);
        }

        /// <summary>
        /// Calculates the cross correlation of a given signal with itself when the signal is shifted
        /// </summary>
        /// <param name="xSeries">The signal to auto correlate.</param>
        /// <param name="maxDelay">The maximum delay at which to shift the signal</param>
        /// <param name="mode">How to process the correlation for edge cases</param>
        /// <returns>A collection of correlation results outlining the correlation coefficient at multiple delays</returns>
        public static IEnumerable<CorrelationResult> AutoCorrelation(IEnumerable<double> xSeries, int maxDelay, CorrelationMode mode = CorrelationMode.IgnoreOutOfRange)
        {
            // Use Paul Bourke Cross Correlation function
            return PaulBourkeCrossCorrelation(xSeries, xSeries, maxDelay, mode);
        }

        /// <summary>
        /// Calculates the cross correlation of two signals as per Paul Bourke's implementation for a single shift delay
        /// </summary>
        /// <param name="xSeries">Signal A to correlate against. Stays constant throughout correlation process</param>
        /// <param name="ySeries">Signal B to correlate with. Signal is shifted by a given delay to test how well Signal B correlates with Signal A</param>
        /// <param name="delay">How far to shift Signal B when computing correlation coefficientThe </param>
        /// <param name="mode">How to process the correlation for edge cases</param>
        /// <returns>A the correlation coefficient between the two signals when Signal B is shifted by the given delay</returns>
        public static double PaulBourkeCrossCorrelationCoefficient(IEnumerable<double> xSeries, IEnumerable<double> ySeries, int delay, CorrelationMode mode = CorrelationMode.IgnoreOutOfRange)
        {
            // TODO: Swap xSeries and ySeries if xSeries is shorter than ySeries?

            // http://paulbourke.net/miscellaneous/correlate/

            // Calculate the mean (or average) of the two series
            double meanX = xSeries.Average();
            double meanY = ySeries.Average();

            // Calculate the Standard Deviation for the two series
            double standardDevX = xSeries.StandardDeviation();
            double standardDevY = ySeries.StandardDeviation();

            // Calculate the denominator
            double denominator = Math.Sqrt(standardDevX * standardDevY);

            // If denominator is zero, return empty list
            if (Math.Abs(denominator) < double.Epsilon) return 0;

            // Convert IEnumerable to array to provide indexing capability
            double[] xArray = xSeries.ToArray();
            double[] yArray = ySeries.ToArray();

            // Calculate the correlation series
            double correlationSeries = CalculateCorrelationSeries(xArray, yArray, delay, meanX, meanY, mode);

            // Calculate the Correlation Coefficient
            double correlationCoefficient = correlationSeries / denominator;
            return correlationCoefficient;
        }

        /// <summary>
        /// Calculates the cross correlation of two signals as per Paul Bourke's implementation
        /// </summary>
        /// <param name="xSeries">Signal A to correlate against. Stays constant throughout correlation process</param>
        /// <param name="ySeries">Signal B to correlate with. Signal is shifted multiple times to determine delay at which the functions achieve strong correlation</param>
        /// <param name="mode">How to process the correlation for edge cases</param>
        /// <returns>A collection of correlation results outlining the correlation coefficient at multiple delays</returns>
        public static IEnumerable<CorrelationResult> PaulBourkeCrossCorrelation(IEnumerable<double> xSeries, IEnumerable<double> ySeries, CorrelationMode mode = CorrelationMode.IgnoreOutOfRange)
        {
            return PaulBourkeCrossCorrelation(xSeries, ySeries, Math.Min(xSeries.Count(), ySeries.Count()), mode);
        }

        /// <summary>
        /// Calculates the cross correlation of two signals as per Paul Bourke's implementation
        /// </summary>
        /// <param name="xSeries">Signal A to correlate against. Stays constant throughout correlation process</param>
        /// <param name="ySeries">Signal B to correlate with. Signal is shifted multiple times to determine delay at which the functions achieve strong correlation</param>
        /// <param name="maxDelay">The maximum delay at which to shift Signal B</param>
        /// <param name="mode">How to process the correlation for edge cases</param>
        /// <returns>A collection of correlation results outlining the correlation coefficient at multiple delays</returns>
        public static IEnumerable<CorrelationResult> PaulBourkeCrossCorrelation(IEnumerable<double> xSeries, IEnumerable<double> ySeries, int maxDelay, CorrelationMode mode = CorrelationMode.IgnoreOutOfRange)
        {
            // TODO: Swap xSeries and ySeries if xSeries is shorter than ySeries?
            
            // http://paulbourke.net/miscellaneous/correlate/

            // Calculate the mean (or average) of the two series
            double meanX = xSeries.Average();
            double meanY = ySeries.Average();

            // Calculate the Standard Deviation for the two series
            double standardDevX = xSeries.StandardDeviation();
            double standardDevY = ySeries.StandardDeviation();

            // Calculate the denominator
            // Sqrt(sdX) * Sqrt(sdY)
            // Sqrt(sdX * sdY)
            double denominator = Math.Sqrt(standardDevX * standardDevY);
            
            // If denominator is zero, return empty list
            if (Math.Abs(denominator) < double.Epsilon)
            {
                // Return the correlation list filled with correlation values of 0
                List<CorrelationResult> res = new List<CorrelationResult>();
                for (int i = -maxDelay; i < maxDelay; i++)
                    res.Add(new CorrelationResult() { Delay = i, Correlation = 0 });
                return res;
            }

            // According to Paul Bourke, there are three ways to calculate the cross correlation series when shifting the second series
            // and calculating y(i - d):
            // 1. Ignore the index values if they are out of range
            // 2. Treat as zero
            // 3. Treat the series as circular and wrap the index around
            
            /*
             * There is the issue of what to do when the index into the series is less than 0 or greater
             * than or equal to the number of points. (i-d < 0 or i-d >= N)
             * The most common approaches are to either ignore these points or assuming the series x and y are zero for i < 0 and i >= N.
             * In many signal processing applications the series is assumed to be circular in which case the
             * out of range indexes are "wrapped" back within range, i.e.: x(-1) = x(N-1), x(N+5) = x(5) etc
             */

            // Convert IEnumerable to array to provide indexing capability
            double[] xArray = xSeries.ToArray();
            double[] yArray = ySeries.ToArray();

            // Calculate the correlation series
            // This will generate a list that is 2*MaxDelay
            List<CorrelationResult> correlationResults = new List<CorrelationResult>();
            for (int delay = -maxDelay; delay < maxDelay; delay++)
            {
                double correlationSeries = CalculateCorrelationSeries(xArray, yArray, delay, meanX, meanY, mode);
                double correlationCoefficient = correlationSeries / denominator;
                correlationResults.Add(new CorrelationResult { Delay = delay, Correlation = correlationCoefficient });
            }

            return correlationResults;
        }

        private static double CalculateCorrelationSeries(double[] xSeries, double[] ySeries, int delay, double meanX, double meanY, CorrelationMode mode)
        {
            double correlationSeries = 0;

            // ASSUMPTION: We will always shift the ySeries
            // TODO: What if xSeries is shorter than ySeries? Do we need to pass them in with the longer series first?
            for (int i = 0; i < ySeries.Length; i++)
            {
                int delayedIndex = i + delay; // Start lagged, then eventually lead

                // CorrelationVal will be null if we use CorrelationMode.IgnoreOutOfRange and the index was out of range
                double? correlationVal = CalculateCorrelationSeriesValue(xSeries, ySeries, i, delayedIndex, meanX, meanY, mode);
                if (correlationVal != null) correlationSeries += correlationVal.Value;
            }

            return correlationSeries;
        }

        private static double? CalculateCorrelationSeriesValue(double[] xSeries, double[] ySeries, int index, int delayedIndex, double meanX, double meanY, CorrelationMode mode)
        {
            // Calculate correlation value for the index
            double? correlationVal = null;
            switch (mode)
            {
                case CorrelationMode.IgnoreOutOfRange: correlationVal = IgnorableCorrelationValue(xSeries, ySeries, index, delayedIndex, meanX, meanY); break;
                case CorrelationMode.TreatAsZero: correlationVal = ZeroableCorrelationValue(xSeries, ySeries, index, delayedIndex, meanX, meanY); break;
                case CorrelationMode.Circularise: correlationVal = CircularCorrelationValue(xSeries, ySeries, index, delayedIndex, meanX, meanY); break;
                default: break;
            }
            return correlationVal;
        }

        private static double? IgnorableCorrelationValue(double[] xSeries, double[] ySeries, int index, int delayedIndex, double meanX, double meanY)
        {
            return (delayedIndex < 0 || delayedIndex > ySeries.Length)
                ? (double?)null
                : ActualCorrelationValue(xSeries, ySeries, index, delayedIndex, meanX, meanY);
        }

        private static double ZeroableCorrelationValue(double[] xSeries, double[] ySeries, int index, int delayedIndex, double meanX, double meanY)
        {
            return (delayedIndex < 0 || delayedIndex > ySeries.Length)
                ? (xSeries[index] - meanX) * (0 - meanY)
                : ActualCorrelationValue(xSeries, ySeries, index, delayedIndex, meanX, meanY);
        }

        private static double CircularCorrelationValue(double[] xSeries, double[] ySeries, int index, int delayedIndex, double meanX, double meanY)
        {
            return (delayedIndex < 0 || delayedIndex > ySeries.Length)
                ? (xSeries[index] - meanX) * (ySeries[delayedIndex.Modulo(ySeries.Length)] - meanY)
                : ActualCorrelationValue(xSeries, ySeries, index, delayedIndex, meanX, meanY);
        }

        private static double ActualCorrelationValue(double[] xSeries, double[] ySeries, int index, int delayedIndex, double meanX, double meanY)
        {
            return (xSeries[index] - meanX) * (ySeries[delayedIndex] - meanY);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSignals
{
    /// <summary>
    /// Defines the possible modes of correlation as described by Paul Bourke at 
    /// http://paulbourke.net/miscellaneous/correlate/
    /// </summary>
    public enum CorrelationMode
    {
        /// <summary>
        /// Ignore values for the shifted series where the index is out of bounds
        /// </summary>
        IgnoreOutOfRange,
        
        /// <summary>
        /// Treat values as zero for the shifted series where the index is out of bounds
        /// </summary>
        TreatAsZero,

        /// <summary>
        /// For the shifted series, where the index is out of bounds, wrap (modulo) the index around
        /// </summary>
        Circularise
    }
}

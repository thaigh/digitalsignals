﻿using System;
using NUnit.Framework;

namespace DigitalSignals.Tests
{
    [TestFixture]
    public class IntExtensionsTests
    {
        [Test]
        [TestCase(7, 5, 2)]
        [TestCase(6, 5, 1)]
        [TestCase(5, 5, 0)]
        [TestCase(4, 5, 4)]
        [TestCase(3, 5, 3)]
        [TestCase(2, 5, 2)]
        [TestCase(1, 5, 1)]
        [TestCase(0, 5, 0)]
        [TestCase(-1, 5, 4)]
        [TestCase(-2, 5, 3)]
        [TestCase(-3, 5, 2)]
        [TestCase(-4, 5, 1)]
        [TestCase(-5, 5, 0)]
        [TestCase(-6, 5, 4)]
        [TestCase(-7, 5, 3)]
        public void Modulo(int val, int mod, int expected)
        {
            int actual = val.Modulo(mod);
            Assert.AreEqual(expected, actual);
        }
    }
}
